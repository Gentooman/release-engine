package com.gentooman.gdxtest;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public interface GraphicObject 
{
	public void draw(SpriteBatch batch, Vector2 position, float rotation);
	
	public void flip(boolean x, boolean y);
	
	public float getSizeX();
	
	public float getSizeY();
}
