package com.gentooman.gdxtest;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Rectangle;

public class GameCamera extends OrthographicCamera 
{
	private static GameCamera ref = new GameCamera();
	private Vector2 mapDimensions, defaultSize;
	private Rectangle positionArea;
	
	private GameCamera()
	{
		mapDimensions = new Vector2();
		defaultSize = new Vector2();
		positionArea = new Rectangle();
	}
	
	//Private method to modify all settings in bulk and save space
	private void modifySettings(boolean orthoBoolean, float viewportWidth, float viewportHeight, Vector2 mapSize)
	{
		viewportWidth = Math.min(viewportWidth, mapSize.x);
		viewportHeight = Math.min(viewportHeight, mapSize.y);
		setToOrtho(orthoBoolean, viewportWidth, viewportHeight);
		positionArea.set(viewportWidth/2, 
                       viewportHeight/2, 
                       mapSize.x - (viewportWidth/2), 
                       mapSize.y - (viewportHeight/2));
		
		//DEBUG
		System.out.println("positionArea: " + positionArea + " - " + "vectorLimit: " + "["+positionArea.width+":"+positionArea.height+"]");
	}
	
	public void setDefaults(boolean orthoBoolean, float viewportWidth, float viewportHeight, Vector2 mapSize)
	{
		defaultSize.set(viewportWidth, viewportHeight);
		mapDimensions.set(mapSize);
		modifySettings(orthoBoolean, viewportWidth, viewportHeight, mapSize);
		update();
		
		//DEBUG
		System.out.println(position + " - " + defaultSize);
		System.out.println(viewportWidth + ", " + viewportHeight);
	}
	
	//Sets the center of the camera given float x and float y. Also checks if
	//the camera is within the boundaries of the map, then repositions
	public void setPos(Vector2 cameraPos)
	{
		if (!positionArea.contains(cameraPos))
		{
			if (cameraPos.x < positionArea.x) {cameraPos.x = positionArea.x;}
			else if(cameraPos.x > positionArea.width) {cameraPos.x = positionArea.width;}
			if (cameraPos.y < positionArea.y) {cameraPos.y = positionArea.y;}
			else if (cameraPos.y > positionArea.height) {cameraPos.y = positionArea.height;}
		}
		
		position.set(cameraPos.x, cameraPos.y, 0f);
	}
	
	//Resizes the viewport of the camera given float width and float height
	public void setCameraSize(float viewportWidth, float viewportHeight)
	{
		modifySettings(false, viewportWidth, viewportHeight, mapDimensions);
	}
	
	//Scales the camera using the default dimensions
	public void setCameraSize(float scale)
	{
		modifySettings(false, defaultSize.x * scale, defaultSize.y * scale, mapDimensions);
	}
	
	//Resizes the viewport of the camera to the original size
	public void setCameraSize()
	{
		modifySettings(false, defaultSize.x, defaultSize.y, mapDimensions);
	}
	
	//GETTERS
	public static GameCamera getInstance()
	{
		if(ref==null) {ref = new GameCamera();}
		return ref;
	}
}