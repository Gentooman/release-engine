package com.gentooman.gdxtest;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileSets;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.maps.MapLayers;
import com.badlogic.gdx.maps.MapProperties;

/* Every map can have a background layer (only an image, stored in "bckg" at 
 * mapProperties, a backDecoLayer, a mainLayer for tiles supposed to be physical, 
 * an objectsLayer where events, sensors and objects are stored and a 
 * frontDecoLayer for overlaying tiles. */

public class GameMap 
{
	private TiledMap map;
	public Texture background;
	public TiledMapTileLayer backDecoLayer, mainLayer, frontDecoLayer;
	private MapObjects mapObjects, mapPhysics;
	private Vector2 mapSize, gravity;
	
	public GameMap(TiledMap mapFile)
	{
		map = mapFile;
		MapLayers mapLayers = map.getLayers();
		MapProperties mapProperties = map.getProperties();
		background = new Texture(Gdx.files.internal("map/bckg/" + mapProperties.get("bckg", String.class)));
		backDecoLayer = (TiledMapTileLayer) mapLayers.get("backDeco");
		mainLayer = (TiledMapTileLayer) mapLayers.get("main");
		frontDecoLayer = (TiledMapTileLayer) mapLayers.get("frontDeco");
		mapObjects = mapLayers.get("objects").getObjects();
		mapPhysics = mapLayers.get("physics").getObjects();
		mapSize = new Vector2(mainLayer.getWidth(), mainLayer.getHeight());
		gravity = new Vector2(Float.parseFloat(mapProperties.get("gravityX", "0", String.class)), 
				                Float.parseFloat(mapProperties.get("gravityY", "-10", String.class)));
	}
	
	public void dispose()
	{
		map.dispose();
		background.dispose();
	}
	
	//GETTERS
	public TiledMap getMap()
	{
		return map;
	}
	
	public MapObjects getMapObjects()
	{
		return mapObjects;
	}
	
	public Vector2 getGravity()
	{
		return gravity;
	}
	
	public Vector2 getMapSize()
	{
		return mapSize;
	}
	
	public MapObjects getPhysicsLayer()
	{
		return mapPhysics;
	}
}
