package com.gentooman.gdxtest;

import java.util.Iterator;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.objects.PolygonMapObject;
import com.badlogic.gdx.maps.objects.PolylineMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTile.BlendMode;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.math.Polyline;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

public class GameMapLoader 
{
	private static GameMapLoader ref;
	
	private GameMapLoader()
	{
		
	}
	
	//Not enough time to implement them yet, maybe in upcoming versions
	enum Shapes
	{
		RECTANGLE,
		TRIANGLE,
		CIRCLE
	}
	
	public void loadMap(GameMap map, World world, Array<Body> array, int warpID, float scale)
	{
		loadMapPhysics(map.getPhysicsLayer(), world, map.getMapSize(), scale);
		loadObjects(map, world, array, warpID, scale);
		replaceTiles(map);
	}
	
	private void replaceTiles(GameMap map)
	{
		Iterator<TiledMapTileSet> tileSets = map.getMap().getTileSets().iterator();
		while(tileSets.hasNext())
		{
			Iterator<TiledMapTile> tiles = tileSets.next().iterator();
			while(tiles.hasNext())
			{
				TiledMapTile tile = tiles.next();
				MapProperties tProperties = tile.getProperties();
				if(tProperties.get("blend", "", String.class).equals("false"))
				{
					tile.setBlendMode(BlendMode.NONE);
				}
				if(tProperties.get("animation", String.class) != null)
				{
					//I will set this up later, there is no need for animated tiles right now
				}
			}
		}
	}
	
	private void loadMapPhysics(MapObjects physics, World world, Vector2 size, float scale)
	{
		Iterator<MapObject> mapObjects = physics.iterator();
		BodyDef groundDef = new BodyDef();
		Body groundBody = world.createBody(groundDef);
		while(mapObjects.hasNext())
		{
			MapObject e = mapObjects.next();
			PolylineMapObject eShape = (PolylineMapObject) e;
			MapProperties eProperties = e.getProperties();
			float[] vertices = eShape.getPolyline().getTransformedVertices();
			for(int i = 0; i < vertices.length; i++)
			{
				vertices[i] *= scale;
			}
			ChainShape polyline = new ChainShape();
			polyline.createChain(vertices);
			groundBody.createFixture(createFixtureDef((Shape) polyline, eProperties));
		}
	}
	
	private void loadObjects(GameMap map, World world, Array<Body> array, int warpID, float scale)
	{
		Iterator<MapObject> i = map.getMapObjects().iterator();
		while(i.hasNext())
		{
			MapObject e = i.next();
			Rectangle eRectangle;
			try
			{
				RectangleMapObject t = (RectangleMapObject) e;
				eRectangle = t.getRectangle();
			}
			catch(ClassCastException exception)
			{
				PolygonMapObject t = (PolygonMapObject) e;
				eRectangle = t.getPolygon().getBoundingRectangle();
			}
			eRectangle.set(eRectangle.x * scale, eRectangle.y * scale, 
					         eRectangle.width * scale, eRectangle.height * scale);
			MapProperties eProperties = e.getProperties();
			
			switch(ObjectType.valueOf(eProperties.get("type", String.class)))
			{
				case WARP:
					createWarp(eProperties, eRectangle, world, warpID, array);
					break;
					
				case SENSOR:
					createSensor(eProperties, eRectangle, world);
					break;
					
				case SHAPE:
					createShape(eProperties, eRectangle, world, array, scale);
					break;
					
				default:
					createSensor(eProperties, eRectangle, world);
					break;
			}
		}
	}
	
	private void createWarp(MapProperties eProperties, Rectangle eRectangle, World world, int warpID, Array<Body> array)
	{
		if(Integer.parseInt(eProperties.get("warpID", String.class)) == warpID)
		{
			Player player = Player.getInstance();
			player.spawn(world, new Vector2(eRectangle.x + (eRectangle.width/2),
					                          eRectangle.y + (player.getSizeY()/2)),
			             array);
		}
		BodyDef def = new BodyDef();
		if(eProperties.get("exit", String.class).equals("right"))
		{
			def.position.set(eRectangle.x + eRectangle.width, 
			         		  eRectangle.y + (eRectangle.height/2));
		}
		else
		{
			def.position.set(eRectangle.x, 
	         		        eRectangle.y + (eRectangle.height/2));
		}
		Body warpBox = world.createBody(def);
		FixtureDef fixDef = new FixtureDef();
		fixDef.isSensor = true;
		fixDef.shape = createBoxShape(0, eRectangle.height);
		warpBox.createFixture(fixDef);
		warpBox.setUserData(ObjectScripts.getScript(new Object[]{"WARP", 
				                                                   (String) eProperties.get("warpTo", String.class), 
				                                                   Integer.parseInt(eProperties.get("warpToID", String.class))}));
		fixDef.shape.dispose();
	}
	
	private void createSensor(MapProperties eProperties, Rectangle eRectangle, World world)
	{
		BodyDef def = new BodyDef();
		def.position.set(eRectangle.x + (eRectangle.width/2), 
				           eRectangle.y + (eRectangle.height/2));
		Body collisionBox = world.createBody(def);
		FixtureDef fixDef = new FixtureDef();
		fixDef.isSensor = true;
		fixDef.shape = createBoxShape(eRectangle.width, eRectangle.height);
		collisionBox.createFixture(fixDef);
		collisionBox.setUserData(ObjectScripts.getScript(new Object[]{eProperties.get("script", String.class)}));
		fixDef.shape.dispose();
	}
	
	private void createShape(MapProperties eProperties, Rectangle eRectangle, World world, Array<Body> array, float scale)
	{
		BodyDef def = new BodyDef();
		/* Note that Y has an extra unit. Tiled has a bug where tiles used as objects
		 * have an offset of one unit. If this bug is solved, this should be modified.*/
		def.position.set(eRectangle.x + (eRectangle.width/2), 
				         eRectangle.y + (eRectangle.height/2) + 1);
		def.type = BodyType.DynamicBody;
		Body shapeBox = world.createBody(def);
		Shape shape = (Shape) createBoxShape(eRectangle.width - scale, eRectangle.height - scale);
		shapeBox.createFixture(createFixtureDef(shape, eProperties)); 
		shapeBox.setUserData(GraphicObjectLoader.getInstance().loadObject("testbox"));
		array.add(shapeBox);
		shape.dispose();
	}
	
	private PolygonShape createBoxShape(float width, float height)
	{
		PolygonShape area = new PolygonShape();
		area.setAsBox(width/2, height/2);
		return area;
	}
	
	private FixtureDef createFixtureDef(Shape shape, MapProperties eProperties)
	{
		FixtureDef fixDef = new FixtureDef();
		fixDef.shape = shape;
		fixDef.density = Float.parseFloat(eProperties.get("density", "0.5", String.class));
		fixDef.friction = Float.parseFloat(eProperties.get("friction", "0.5", String.class));
		fixDef.restitution = Float.parseFloat(eProperties.get("restitution", "0.2", String.class));
		return fixDef;
	}
	
	//GETTERS
	public static GameMapLoader getInstance()
	{
		if(ref==null) {ref = new GameMapLoader();}
		return ref;
	}
}
