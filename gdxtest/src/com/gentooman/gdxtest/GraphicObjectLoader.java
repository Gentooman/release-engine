package com.gentooman.gdxtest;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.XmlReader;
import com.badlogic.gdx.utils.XmlReader.Element;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class GraphicObjectLoader 
{	
	private static GraphicObjectLoader ref;
	private Hashtable<String, Texture> loadedTextures;
	private Element xmlFile;
	
	private GraphicObjectLoader()
	{
		
	}
	
	public void create()
	{
	   loadedTextures = new Hashtable<String, Texture>();
      try
      {
         xmlFile = new XmlReader().parse(Gdx.files.internal("definition/GraphicDefinitions.xml"));
      }
      catch(IOException exception)
      {
         exception.printStackTrace();
      }
	}
	
	public GraphicObject loadObject(String objectName)
	{
		GraphicObject gameObject = null;
		
		Element objectDefinition = xmlFile.getChildByName(objectName);
			
		if(objectDefinition.getChildByName("animations") != null)
		{
			gameObject = makeAnimatedObject(objectDefinition);
		}
		else
		{
			gameObject = makeTextureObject(objectDefinition);
		}
		
		return gameObject;
	}
	
	private TextureObject makeTextureObject(Element objectDefinition)
	{
		TextureObject textureObject;
		
		if(objectDefinition.getChildByName("textureregion") != null)
		{
			Element regionElement = objectDefinition.getChildByName("textureregion");
			textureObject = new TextureObject(new TextureRegion(loadTexture(objectDefinition.get("texture")),
					                                              regionElement.getIntAttribute("startX"),
                                                             regionElement.getIntAttribute("startY"),
                                                             regionElement.getIntAttribute("width"),
                                                             regionElement.getIntAttribute("height")),
                                           objectDefinition.getFloat("scale")); 
		}
		else
		{
			textureObject = new TextureObject(loadTexture(objectDefinition.get("texture")), 
					                            objectDefinition.getFloat("scale"));
		}
		
		return textureObject;
	}
	
	private AnimatedObject makeAnimatedObject(Element objectDefinition)
	{
		Element animationsElement = objectDefinition.getChildByName("animations");
		AnimatedObject animatedObject = new AnimatedObject();
		
		Texture texture = loadTexture(objectDefinition.get("texture"));
		
		if(!animationsElement.getBooleanAttribute("irregular", false))
		{
		   animatedObject.setAnimatedObject(texture,
		                                    animationsElement.getIntAttribute("rows"),
		                                    animationsElement.getIntAttribute("columns"));
		   Iterator<Element> animationsIterator = animationsElement.getChildrenByName("animation").iterator();
		   while(animationsIterator.hasNext())
		   {
   		   Element e = animationsIterator.next();
	         animatedObject.addAnimation(e.getAttribute("name"),
	                                     e.getIntAttribute("start"),
	                                     e.getIntAttribute("frames"),
	                                     e.getFloatAttribute("speed"));
	      }
		}
		else
		{
		   Array<Element> animationElements = animationsElement.getChildrenByName("animation");
		   String[] names = new String[animationElements.size];
		   float[][] regions = new float[animationElements.size][];
		   int totalFrames = 0;
		   for(int i = 0; i < animationElements.size; i++)
		   {
		      Element e = animationElements.get(i);
		      names[i] = e.getAttribute("name");
		      regions[i] = new float[] {e.getFloatAttribute("startX", 0f),
		                                e.getFloatAttribute("startY", 0f),
		                                e.getFloatAttribute("width"),
		                                e.getFloatAttribute("height"),
		                                e.getFloatAttribute("columns", 1f),
		                                e.getFloatAttribute("rows", 1f),
		                                e.getFloatAttribute("frames", 1f),
		                                e.getFloatAttribute("speed", 30f)};
		      totalFrames += e.getIntAttribute("frames", 1);
		   }
		   
		   animatedObject.setAnimatedObject(texture, names, regions, totalFrames);
		}
		
		animatedObject.playAnimation(animationsElement.getChild(0).getAttribute("name"));
		animatedObject.setScale(objectDefinition.getFloat("scale"));
		return animatedObject;
	}
	
	public Texture loadTexture(String texturePath)
	{
		if(!loadedTextures.containsKey(texturePath))
		{
			Texture texture = new Texture(texturePath);
			texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			loadedTextures.put(texturePath, texture);
		}
		
		return loadedTextures.get(texturePath);
	}
	
	public void dispose()
	{
		Enumeration<Texture> e = loadedTextures.elements(); 
		while(e.hasMoreElements())
		{
			e.nextElement().dispose();
		}
		loadedTextures.clear();
	}
	
	//GETTERS
	public static GraphicObjectLoader getInstance()
	{
		if(ref==null){ref = new GraphicObjectLoader();}
		return ref;
	}
}
