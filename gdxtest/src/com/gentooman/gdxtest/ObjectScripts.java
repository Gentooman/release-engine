package com.gentooman.gdxtest;

public class ObjectScripts 
{
	enum ScriptsList
	{
		WARP,
		TEST
	}
	
	public static Script getScript(Object[] parameters)
	{
		Script script;
		switch(ScriptsList.valueOf((String) parameters[0]))
		{
			case WARP:
				script = warp((String) parameters[1],(Integer) parameters[2]);
				break;
			case TEST:
				script = test();
				break;
			default:
				script = test();
				break;
		}
		
		return script;
	}
	
	private static Script warp(String map, int id)
	{
		Script warpScript = new Script()
		{
		   private String warpTo;
		   private int warpToID;
		
		   public Script init(String map, int id)
		   {
		      warpTo = map;
		      warpToID = id;
		      return this;
		   }
		
		   public void contactScript()
		   {
			
		   }
		
		   public void separationScript()
		   {
			
		   }
		}.init(map, id);
		
		return warpScript;
	}
	
	private static Script test()
	{
	   Script testScript = new Script()
	   {
	      private boolean flag = true;
		
	      public void contactScript()
	      {
	         if(flag==true)
	         {
	            System.out.println("Script successful!");
	            flag = false;
	         }
	      }
		
	      public void separationScript()
	      {
	         flag = true;
	      }
	   };
	   
	   return testScript;
	}
}
