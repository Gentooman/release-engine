package com.gentooman.gdxtest;

import com.badlogic.gdx.physics.box2d.Body;

public interface Script 
{
	public void contactScript();
	
	public void separationScript();
}
