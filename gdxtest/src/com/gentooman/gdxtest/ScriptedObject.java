package com.gentooman.gdxtest;

import com.badlogic.gdx.physics.box2d.Body;

public class ScriptedObject 
{
	private ObjectType type;
	private Script script;
	
	public ScriptedObject(String map, int id)
	{
		type = ObjectType.WARP;
	}
	
	public ScriptedObject(String scriptName)
	{
		script = ObjectScripts.getScript(new Object[]{scriptName});
		type = ObjectType.SENSOR;
	}
	
	public void onContact(Body e)
	{
		script.contactScript();
	}
	
	public void onRelease(Body e)
	{
		script.separationScript();
	}
}
