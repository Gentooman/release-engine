package com.gentooman.gdxtest;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.utils.Array;

//DEBUG
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;

public class GameWorld 
{
	private static GameWorld ref;
	protected GameCamera camera = GameCamera.getInstance();
	private GraphicObjectLoader objectLoader = GraphicObjectLoader.getInstance();
	private GameMapLoader mapLoader = GameMapLoader.getInstance();
	private World world;
	private final float UNIT_SCALE = 1f/32f;
	
	private OrthogonalTiledMapRenderer mapRenderer;
	private SpriteBatch batch;
	private GameMap currentMap;
	private Array<Body> mapBodies;
	
	private Box2DDebugRenderer debugBox2D;

	private GameWorld()
	{
	   
	}
	
	public void setMap(String mapFile)
	{
	   currentMap = new GameMap(new TmxMapLoader().load("map/" + mapFile));
		System.out.println(currentMap.getGravity());
		world = new World(currentMap.getGravity(), true);
		debugBox2D = new Box2DDebugRenderer();
		batch = new SpriteBatch();
		mapRenderer = new OrthogonalTiledMapRenderer(currentMap.getMap(), 
		                                               getUnitScale(),
		                                               batch);
		
		mapBodies = new Array<Body>();
		
		camera.setDefaults(false, 
				             ((float) Gdx.graphics.getWidth()/Gdx.graphics.getHeight())*10, 10,
				             currentMap.getMapSize());
		camera.setCameraSize(16);
		mapLoader.loadMap(currentMap, world, mapBodies, 0, UNIT_SCALE);
		
		Array<Body> currentBodies = new Array<Body>();
		world.getBodies(currentBodies);
	}
	
	public void render()
	{
		camera.update();
		mapRenderer.setView(camera);
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
			drawBackLayers();
			drawObjects();
			drawFrontLayers();
		batch.end();
		debugBox2D.render(world, camera.combined);
		world.step(1/45f, 6, 2);
	}
	
	private void drawBackLayers()
	{
	   batch.disableBlending();
	   batch.draw(currentMap.background, 0, 0);
	   batch.enableBlending();
	   mapRenderer.renderTileLayer(currentMap.backDecoLayer);
	}
	
	private void drawFrontLayers()
	{
	   mapRenderer.renderTileLayer(currentMap.mainLayer);
	   //mapRenderer.renderTileLayer(currentMap.frontDecoLayer);
	}
	
	private void drawObjects()
	{
		for(Body a: mapBodies)
		{
			GraphicObject d = (GraphicObject) a.getUserData();
			d.draw(batch, a.getPosition(), a.getAngle());
		}
	}
	
	public void dispose()
	{
		currentMap.dispose();
		batch.dispose();
		objectLoader.dispose();
		world.dispose();
	}
	
	//GETTERS
	public static GameWorld getInstance()
	{
		if(ref==null) {ref = new GameWorld();}
		return ref;
	}
	
	public World getWorld()
	{
		return world;
	}
	
	public GameMap getCurrentMap()
	{
		return currentMap;
	}
	
	public float getUnitScale()
	{
		return UNIT_SCALE;
	}
}