package com.gentooman.gdxtest;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import java.util.Hashtable;

public class AnimatedObject implements GraphicObject 
{
	private float stateTime = 0f;
	private float[] scaledSize;
	private boolean flipX, flipY;
	private TextureRegion[] framesArray;
	private Hashtable<String, Animation> animationsTable;
	private Animation currentAnimation;
	private String currentAnimationKey;
	
	public AnimatedObject()
	{
	   animationsTable = new Hashtable<String, Animation>();
      flipX = false;
      flipY = false;
      scaledSize = new float[2];
	}
	
	public void draw(SpriteBatch batch, Vector2 position, float rotation)
	{
		batch.draw(getCurrentFrame(), position.x-(getSizeX()/2f), position.y-(getSizeY()/2f), 
                 getSizeX()/2, getSizeY()/2, getSizeX(), getSizeY(), 1, 1, MathUtils.radiansToDegrees * rotation);
	}

	public void addAnimation(String name, int start, int frames, float speed)
	{
		TextureRegion[] f = new TextureRegion[frames];
		
		for(int i = 0; i < frames; i++)
		{
		   f[i] = framesArray[start+i];
		}
		
		animationsTable.put(name, new Animation(1/speed, f));
	}
	
	public void playAnimation(String name)
	{
		try
		{
			currentAnimation = animationsTable.get(name);
			
			if((currentAnimation.getKeyFrame(0).isFlipX() != flipX) || (currentAnimation.getKeyFrame(0).isFlipY() != flipY))
			{
				for(float i = currentAnimation.frameDuration/2; i < currentAnimation.animationDuration; i+=currentAnimation.frameDuration)
				{
					currentAnimation.getKeyFrame(i).flip((currentAnimation.getKeyFrame(i).isFlipX() != flipX), 
							                               (currentAnimation.getKeyFrame(i).isFlipY() != flipY));
				}
			}

			currentAnimationKey = name;
			stateTime = 0f;
		}
		catch(NullPointerException e)
		{
			System.err.println("Caught NullPointerException: " + e.getMessage());
		}
	}
	
	//SETTERS
	
	//Sets the AnimatedObject for a equally distributed animation texture
	public void setAnimatedObject(Texture texture, int rows, int columns)
	{
      TextureRegion[][] textureMatrix = TextureRegion.split(texture,
                                                            texture.getWidth() / columns,
                                                            texture.getHeight() / rows);
      framesArray = new TextureRegion[columns * rows];
      
      int index = 0;
      for(int i = 0; i < rows; i++)
      {
         for(int j = 0; j < columns; j++)
         {
            framesArray[index++] = textureMatrix[i][j];
         }
      }
	}
	
	//Sets the AnimatedObject and animations for a not equally distributed texture
	public void setAnimatedObject(Texture texture, String[] names, float[][] regions, int totalFrames)
	{
	   framesArray = new TextureRegion[totalFrames];
	   int index = 0;
	   for(int i = 0; i < names.length; i++)
	   {
	      TextureRegion framesRegion = new TextureRegion(texture, 
	                                                     (int) regions[i][0], (int) regions[i][1], 
	                                                     (int) regions[i][2], (int) regions[i][3]);
	      TextureRegion framesMatrix[][] = framesRegion.split(framesRegion.getRegionWidth()/((int) regions[i][4]), 
	                                                          framesRegion.getRegionHeight()/((int) regions[i][5]));
	      
	      int animationIndex = 0;
	      for(int j = 0; j < regions[i][5]; j++)
	      {
	         for(int k = 0; k < regions[i][4]; k++)
	         {
	           if(regions[i][6] >= animationIndex++)
	            {
	              framesArray[index++] = framesMatrix[j][k];
	            }
	            else {break;}
	         }
	      }
	      
	      addAnimation(names[i], index-animationIndex, animationIndex, regions[i][7]);
	   }
	}
	
	//Copies an AnimatedObject 
	public void setAnimatedObject(AnimatedObject e)
	{
	   stateTime = e.stateTime;
	   scaledSize = e.scaledSize;
	   flipX = e.flipX;
	   flipY = e.flipY;
	   framesArray = e.framesArray;
	   animationsTable = e.animationsTable;
	   currentAnimation = e.currentAnimation;
	   currentAnimationKey = e.currentAnimationKey;
	}
	
	public void setScale(float scale)
	{
		scale /= 32f;
		scaledSize[0] = framesArray[0].getRegionWidth()*scale;
		scaledSize[1] = framesArray[0].getRegionHeight()*scale;
	}
	
	public void flip(boolean x, boolean y)
	{
		flipX = x;
		flipY = y;
		this.playAnimation(currentAnimationKey);
	}
	
	//GETTERS
	public TextureRegion getCurrentFrame()
	{
		stateTime += Gdx.graphics.getDeltaTime();
		TextureRegion region = currentAnimation.getKeyFrame(Math.max(0, stateTime), true);
		return region;
	}
	
	public float getSizeX()
	{
		return scaledSize[0];
	}
	
	public float getSizeY()
	{
		return scaledSize[1];
	}
}
