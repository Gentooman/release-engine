package com.gentooman.gdxtest;

public enum ObjectType 
{
	WARP,
	SENSOR,
	SHAPE,
	ENEMY;
}
