package com.gentooman.gdxtest;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.utils.Array;

public class Player extends AnimatedObject implements GraphicObject
{
	private static Player ref;
	private BodyDef bodyDef;
	private FixtureDef fixtureDef;
	private Body body;
	
	private Player()
	{
		super();
	   GraphicObjectLoader loader = GraphicObjectLoader.getInstance();
		setAnimatedObject((AnimatedObject) loader.loadObject("irregulartest"));
		bodyDef = new BodyDef();
		bodyDef.type = BodyType.DynamicBody;
		bodyDef.fixedRotation = true;
		fixtureDef = new FixtureDef();
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(getSizeX()/2, (getSizeY() - GameWorld.getInstance().getUnitScale())/2);
		fixtureDef.restitution = 0.2f;
		fixtureDef.friction = 0.4f;
		fixtureDef.density = 20;
		fixtureDef.shape = shape;
	}
	
	public void spawn(World world, Vector2 position, Array<Body> array)
	{
		bodyDef.position.set(position);
		body = world.createBody(bodyDef);
		body.createFixture(fixtureDef);
		fixtureDef.shape.dispose();
		body.setUserData(getInstance());
		array.add(body);
	}
	
	//GETTERS
	public static Player getInstance()
	{
		if(ref==null) ref = new Player();
		return ref;
	}
}
