package com.gentooman.gdxtest;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;

public class GdxTest implements ApplicationListener 
{
	private GameWorld world = GameWorld.getInstance();
	private Vector2 position = new Vector2(-200f, -500f);
	
	@Override
	public void create() 
	{		
	   GraphicObjectLoader.getInstance().create();
	   world.setMap("testMap.tmx");
	}

	@Override
	public void dispose() 
	{
		world.dispose();
	}

	@Override
	public void render() 
	{		
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		world.camera.setPos(position);
		world.render();
	}

	@Override
	public void resize(int width, int height) 
	{
		
	}

	@Override
	public void pause() 
	{
		
	}

	@Override
	public void resume() 
	{
		
	}
}
