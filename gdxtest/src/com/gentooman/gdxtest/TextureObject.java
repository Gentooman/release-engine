package com.gentooman.gdxtest;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

public class TextureObject implements GraphicObject 
{	
	private TextureRegion region;
	float[] scaledSize;
	
	public TextureObject()
	{
	   
	}
	
	public TextureObject(TextureRegion region, float scale)
	{
		this.region = region;
		scaledSize = new float[2];
		setScale(scale);
	}
	
	public TextureObject(Texture texture, float scale)
	{
		this.region = new TextureRegion(texture, 0, 0, texture.getWidth(), texture.getHeight());
		scaledSize = new float[2];
		setScale(scale);
	}
	
	public void draw(SpriteBatch batch, Vector2 position, float rotation)
	{
		batch.draw(region, position.x-(getSizeX()/2f), position.y-(getSizeX()/2f), 
                 getSizeX()/2, getSizeY()/2, getSizeX(), getSizeY(), 1, 1, MathUtils.radiansToDegrees * rotation);
	}
	
	public void flip(boolean x, boolean y) 
	{
		region.flip((region.isFlipX() != x), (region.isFlipY() != y));
	}
	
	//SETTERS
	public void setScale(float scale)
	{
	   scale /= 32f;
	   scaledSize[0] = region.getRegionWidth()*scale;
	   scaledSize[1] = region.getRegionHeight()*scale;
	}
	
	//GETTERS
	public float getSizeX()
	{
		return scaledSize[0];
	}
	
	public float getSizeY()
	{
		return scaledSize[1];
	}
}
